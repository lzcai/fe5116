function unit_test_quadroot()

    res0 = min(quadroot(1,2,1) == [-1,-1]);
	res1 = min(quadroot(1,0,-4) == [2,-2]);
	res2 = min(quadroot(1,-3,2) == [2,1]);

	if ( res0 == 0 || res1 == 0 || res2 == 0 )
		error('test failed')
	else
		display('test pass')
    end
end
