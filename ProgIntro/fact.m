function y=fact(n)
   assert(n>=0, ['n must be positive, but you entered: ', num2str(n)] );
   assert(n == uint64(n), 'n must be integer' )
   y = fact2(n);
end


function y=fact2(n)
   if (n == 0)
	  y = 1;
   else
	  y = n * fact2(n - 1);
   end
end
