function rand_test_quadroot()

	for i=1:100
		z = sort(10 * (rand(1,2) - 0.5)); % testing roots in [-5, 5]
		a = 4*rand(1,1);
		c = a*z(1)*z(2);
		b = -a*(z(1)+z(2));

		res = sort( quadroot(a,b,c) );

        % if we check for equality, it always fail
		ok = max(abs(z-res)) < 1e-7;
		%ok = max(z-res) == 1;  // this would fail!

		if ( ~ok )
			[a,b,c],
			z,
			res,
		   error('Houston, we have a problem')
        end
	end

	disp('test pass')
end