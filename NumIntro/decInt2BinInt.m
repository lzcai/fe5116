% decInt2BinInt.m
%
% Author: Fabio Cannizzo
%
% y = dec2bin (x)
%

function m=decInt2BinInt( x, nbits )
   m=zeros(1,nbits);
   p = 1;
   for i=1:nbits
	  t = p*2;
	  if mod(x,t) ~= 0
		 x=x-p;
		 m(nbits+1-i)=1;
	  end
	  p=t;
   end


